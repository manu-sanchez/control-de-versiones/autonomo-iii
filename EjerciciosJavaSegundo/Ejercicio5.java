import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int numero=0;
		
		System.out.println("Introduce un numeros");
            
            numero = sc.nextInt();
            
            
            if (numero < 0 || numero > 9999){
            	
            	System.out.println("Numero no valido");
            	System.out.println("Introduce un numero");
            	numero = sc.nextInt();
            	System.out.println("El n�mero " + numero + " tiene " + Integer.toString(numero).length() + " d�gitos");
            	
            }else{
            	System.out.println("El n�mero " + numero + " tiene " + Integer.toString(numero).length() + " d�gitos");
            }
            
        
	}

}
